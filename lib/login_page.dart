import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_try/home_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage ({Key key}) : super(key : key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  TextEditingController emailInputController;
  TextEditingController pwdInputController;

  @override
  initState() { 
    emailInputController = TextEditingController();
    pwdInputController = TextEditingController();
    super.initState();
  }

  String emailValidator(String value) {
    Pattern pattern = 
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      return 'email format is invalid';
    } else {
      return null;
    }
  }

  String pwdValidator(String value) {
    if (value.length<8) {
      return 'must be longer than 8';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Form(
            key: _loginFormKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Email', hintText: 'toyibseha@gmail.com'
                  ),
                  controller: emailInputController,
                  keyboardType: TextInputType.emailAddress,
                  validator: emailValidator,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Password', hintText: '****'
                  ),
                  controller: pwdInputController,
                  obscureText: true,
                  validator: pwdValidator,
                ),

                RaisedButton(
                  child: Text('Login'),
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  onPressed: () {
                    if (_loginFormKey.currentState.validate()) {
                      FirebaseAuth.instance.signInWithEmailAndPassword(
                        email: emailInputController.text,
                        password: pwdInputController.text
                      ).then((currentUser) => Firestore.instance.collection("users").document(currentUser.user.uid).get().then((DocumentSnapshot result) => Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) => HomePage(
                          title: result["fname"] + "'s Tasks",
                          uid: currentUser.user.uid
                        )
                      )))
                      .catchError((err) => print(err))
                      ).catchError((err) => print(err));
                    }
                  },
                ),

                Text("Don't have an account yet ?"),
                FlatButton(
                  child: Text("Register Here!"),
                  onPressed: () {
                    Navigator.pushNamed(context, "/register");
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}