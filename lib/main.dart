import 'package:flutter/material.dart';
import 'package:firebase_try/splash_page.dart';
import 'package:firebase_try/home_page.dart';
import 'package:firebase_try/login_page.dart';
import 'register_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login Firebase',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashPage(),
      routes: <String, WidgetBuilder>{
        // '/task': (BuildContext context) => TaskPage(title: 'Task'),
        '/home': (BuildContext context) => HomePage(title: 'Home'),
        '/login': (BuildContext context) => LoginPage(),
        '/register': (BuildContext context) => RegisterPage(),
      },
    );
  }
}